import { Collection, Db, MongoClient } from 'mongodb';
import { DbServer } from './db.server';
import { Injectable } from '@nestjs/common';

@Injectable()
export class DbProvider {

  public readonly isReady: Promise<void>;

  public get users(): Collection<any> {
    return this.db.collection('users');
  }

  public get posts(): Collection<any> {
    return this.db.collection('posts');
  }

  private db: Db;

  constructor(private dbServer: DbServer) {
    this.isReady = dbServer.getConnectionString()
      .then(this.handleConnect)
      .then(this.handleDbConnection)
      .then(this.logSuccess);
  }

  private handleConnect = async (connectionString: string): Promise<Db> => {
    const connection = await MongoClient.connect(connectionString, { useUnifiedTopology: true });
    return connection.db();
  };

  private handleDbConnection = async (db: Db) => {
    this.db = db;
  };

  private logSuccess = () => {
    console.log('Db initialized');
  }

}
